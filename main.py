from fastapi import FastAPI, File, UploadFile, responses
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from deta import Drive, Base, App
from datetime import datetime, timedelta

app = App(FastAPI())
app.mount("/static", StaticFiles(directory="static"), name="static")
files = Drive("files")
db = Base('files')


@app.get("/")
def homepage():
    html_content = """
    <html>
        <head>
            <title>Transfer Share</title>
            <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.0.10/css/all.css">
            <link rel="stylesheet" href="/static/styles.css">
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
            <script src="https://use.fontawesome.com/releases/v5.0.10/js/all.js"></script>
        </head>
        <body>
            <div class="wrapper">
  <div class="container">
    <h1>Upload a file</h1>
    <div class="upload-container">
      <div class="border-container">
        <div class="icons fa-4x">
          <i class="fas fa-file-image" data-fa-transform="shrink-3 down-2 left-6 rotate--45"></i>
          <i class="fas fa-file-alt" data-fa-transform="shrink-2 up-4"></i>
          <i class="fas fa-file-pdf" data-fa-transform="shrink-3 down-2 right-6 rotate-45"></i>
        </div>
        <input type="file" id="file-upload">
        <p>Drag and drop files here, or 
          <a href="#" id="file-browser">browse</a> your computer.</p>
      </div>
    </div>
  </div>
</div>
        </body>
    </html>
    """
    return HTMLResponse(content=html_content, status_code=200)


@app.post("/upload/")
def Upload(file: UploadFile = File(...)):
    try:
        print(dir(file))
        print(file.content_type)
        drive_key = files.put(file.filename, file.file)

        db_files = db.insert({
            "filename": drive_key,
            "content_type": file.content_type,
            "expiry_date": (datetime.now() + timedelta(days=1)).strftime('%Y-%m-%d %H:%M')
        })
        pass
    except Exception as e:
        raise(e)
    
    return db_files


@app.get("/download/{name}")
def Download(name):
    try:
        fetch_res = db.fetch({"filename": name})
        if fetch_res.count == 1:
            print(fetch_res.items)
            img_res = fetch_res.items[0]
            img = files.get(name)
            resp = responses.StreamingResponse(img.iter_chunks(), media_type=img_res['content_type'])
            pass
        else:
            resp = "File Not Found"
            pass
        pass
    except Exception as e:
        raise(e)

    return resp


@app.lib.cron()
def cron_job(event):
    try:
        fetch_res = db.fetch()
        for item in fetch_res.items:
            expiry_date = datetime.strptime(item['expiry_date'], '%Y-%m-%d %H:%M')
            if expiry_date < datetime.now():
                db_res = db.delete(item['key'])
                drive_res = files.delete(item['filename'])
                pass
            pass
        pass
    except Exception as e:
        raise(e)
    return "running on a schedule"